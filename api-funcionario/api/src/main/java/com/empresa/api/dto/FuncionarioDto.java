package com.empresa.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FuncionarioDto {

    private String nome;

    private Integer idade;

    private String cpf;

    private String celular;

    private String cargo;

    private String setor;

    private Double salario;

}
