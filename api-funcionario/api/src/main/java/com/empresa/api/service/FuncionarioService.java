package com.empresa.api.service;

import com.empresa.api.dto.FuncionarioDto;
import com.empresa.api.entity.Funcionario;
import com.empresa.api.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    public void inserirFuncionario(FuncionarioDto funcionario){

        Funcionario novoFuncionario = new Funcionario();

        novoFuncionario.setNome(funcionario.getNome());
        novoFuncionario.setIdade(funcionario.getIdade());
        novoFuncionario.setCpf(funcionario.getCpf());
        novoFuncionario.setCargo(funcionario.getCargo());
        novoFuncionario.setCelular(funcionario.getCelular());
        novoFuncionario.setSetor(funcionario.getSetor());
        novoFuncionario.setSalario(funcionario.getSalario());

        funcionarioRepository.save(novoFuncionario);

    }

    public List<FuncionarioDto> buscaFuncionarios(){

        List<FuncionarioDto> listaFuncionarios = new ArrayList<>();

        funcionarioRepository.findAll().forEach(item->{

            FuncionarioDto funcionario = FuncionarioDto
                    .builder()
                    .nome(item.getNome())
                    .cargo(item.getCargo())
                    .setor(item.getSetor())
                    .celular(item.getCelular())
                    .cpf(item.getCpf())
                    .idade(item.getIdade())
                    .salario(item.getSalario())
                    .build();

            listaFuncionarios.add(funcionario);

        });

        return listaFuncionarios;
    }


}
