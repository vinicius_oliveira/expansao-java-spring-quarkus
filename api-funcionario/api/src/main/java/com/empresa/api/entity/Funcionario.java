package com.empresa.api.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
public class Funcionario {

    @Id
    @GeneratedValue
    private Long id;

    private String nome;

    private Integer idade;

    private String cpf;

    private String celular;

    private String cargo;

    private String setor;

    private Double salario;

}
