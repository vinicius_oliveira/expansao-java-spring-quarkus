package com.empresa.api.controller;

import com.empresa.api.dto.FuncionarioDto;
import com.empresa.api.service.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/funcionario")
public class FuncionarioController {

    @Autowired
    private FuncionarioService funcionarioService;

    @GetMapping("/lista")
    public ResponseEntity<List<FuncionarioDto>> buscaProdutosPorLoja(){
        return new ResponseEntity<>(funcionarioService.buscaFuncionarios(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity criaNovoFuncionario(@RequestBody FuncionarioDto funcionario){
        try {
            funcionarioService.inserirFuncionario(funcionario);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
